package kg.geektech.calculatormvcrpn.viewer

import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import kg.geektech.calculatormvcrpn.R
import kg.geektech.calculatormvcrpn.controller.Controller

class Viewer : AppCompatActivity() {
    private val controller: Controller

    init {
        println("  Start viewer Constructor().")
        controller = Controller(this)
        println("  i am Viewer object.")
        println("  End Viewer Constructor().")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        findViewById<Button>(R.id.val1).setOnClickListener(controller)
        findViewById<Button>(R.id.val2).setOnClickListener(controller)
        findViewById<Button>(R.id.val3).setOnClickListener(controller)
        findViewById<Button>(R.id.val4).setOnClickListener(controller)
        findViewById<Button>(R.id.val5).setOnClickListener(controller)
        findViewById<Button>(R.id.val6).setOnClickListener(controller)
        findViewById<Button>(R.id.val7).setOnClickListener(controller)
        findViewById<Button>(R.id.val8).setOnClickListener(controller)
        findViewById<Button>(R.id.val9).setOnClickListener(controller)
        findViewById<Button>(R.id.val0).setOnClickListener(controller)

        findViewById<Button>(R.id.add).setOnClickListener(controller)
        findViewById<Button>(R.id.minus).setOnClickListener(controller)
        findViewById<Button>(R.id.mult).setOnClickListener(controller)
        findViewById<Button>(R.id.div).setOnClickListener(controller)
        findViewById<Button>(R.id.back).setOnClickListener(controller)
        findViewById<Button>(R.id.cancel).setOnClickListener(controller)
        findViewById<Button>(R.id.right_par).setOnClickListener(controller)
        findViewById<Button>(R.id.left_par).setOnClickListener(controller)
        findViewById<Button>(R.id.dot).setOnClickListener(controller)
        findViewById<Button>(R.id.equal).setOnClickListener(controller)
    }

    fun update(answer: String, result: String) {
        val intermediateTv = findViewById<TextView>(R.id.intermediate_result)
        val tvResult = findViewById<TextView>(R.id.result)
        intermediateTv.text = answer
        tvResult.text = result
    }
}

