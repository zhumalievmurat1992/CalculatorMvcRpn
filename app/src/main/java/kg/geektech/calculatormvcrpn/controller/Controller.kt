package kg.geektech.calculatormvcrpn.controller

import android.view.View
import kg.geektech.calculatormvcrpn.R
import kg.geektech.calculatormvcrpn.model.Model
import kg.geektech.calculatormvcrpn.viewer.Viewer

class Controller:View.OnClickListener {

    private val model: Model


    constructor(viewer: Viewer) {

        println("  Start Controller constructor().")
        model = Model(viewer)
        println("  I am Controller object.")
        println("  End Controller constructor().")
    }

    override fun onClick(view: View?) {
        when(view?.id){
            R.id.val1 -> model.doAction("1")
            R.id.val2 -> model.doAction("2")
            R.id.val3 -> model.doAction("3")
            R.id.val4 -> model.doAction("4")
            R.id.val5 -> model.doAction("5")
            R.id.val6 -> model.doAction("6")
            R.id.val7 -> model.doAction("7")
            R.id.val8 -> model.doAction("8")
            R.id.val9 -> model.doAction("9")
            R.id.val0 -> model.doAction("0")

            R.id.left_par -> model.doAction("(")
            R.id.right_par -> model.doAction(")")
            R.id.cancel -> model.clear()
            R.id.back -> model.backSpace()
            R.id.div -> model.doAction("/")
            R.id.mult -> model.doAction("*")
            R.id.minus -> model.doAction("-")
            R.id.add -> model.doAction("+")
            R.id.dot -> model.doAction(".")
            R.id.equal ->model.doAction("=")
        }
        model.doAction(view.toString())
    }
}