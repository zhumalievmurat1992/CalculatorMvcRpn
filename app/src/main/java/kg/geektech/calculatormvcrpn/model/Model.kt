package kg.geektech.calculatormvcrpn.model

import kg.geektech.calculatormvcrpn.model.rpn.ExpressionToRpn
import kg.geektech.calculatormvcrpn.viewer.Viewer

class Model {

    private var viewer: Viewer? = null
    private var tmp: String
    private var rnp: ExpressionToRpn
    private var result: String

    constructor(viewer: Viewer) {
        println("  Start Model constructor().")
        this.viewer = viewer
        println("  I am Model object.")
        println("  End Model constructor().")
        result = ""
        tmp = ""
        rnp = ExpressionToRpn()
    }

    fun clear() {
        tmp = " "
        result = " "
        viewer?.update(tmp, result)
        return
    }

    fun backSpace() {
        val tvClick = tmp.length
        if (tvClick > 0) tmp = tmp.subSequence(0, tmp.length - 1).toString()
        viewer?.update(tmp, result)

    }

    fun doAction(command: String) {
        when (command) {
            "1" -> { tmp += "1" }
            "2" -> { tmp += "2" }
            "3" -> { tmp += "3" }
            "4" -> { tmp += "4" }
            "5" -> { tmp += "5" }
            "6" -> { tmp += "6" }
            "7" -> { tmp += "7" }
            "8" -> { tmp += "8" }
            "9" -> { tmp += "9" }
            "0" -> { tmp += "0" }
            "." -> { tmp += "." }
            "+" -> { tmp += '+' }
            "-" -> { tmp += '-' }
            "/" -> { tmp += '/' }
            "*" -> { tmp += '*' }
            "(" -> { tmp += '(' }
            ")" -> { tmp += ')' }
            "=" -> {
                result = rnp.decide(tmp)
                println("temp  $tmp$command")
            }
        }
        viewer?.update(tmp, result)
    }
}